<%-- 
    Document   : menu
    Created on : Oct 1, 2022, 12:24:52 PM
    Author     : Jong
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>-->

<!-- Menu -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/bootstrap/bootstrap.min.css">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Sistema de Ventas</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="${pageContext.servletContext.contextPath}/index.jsp">
                Inicio
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="${pageContext.servletContext.contextPath}/vistas/cliente.jsp">
                Ejercicios
            </a>
          </li>
        </ul>
               
                <!-- Example single danger button -->
        <div class="btn-group">
          <button type="button" class="btn btn-danger dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
             <%= usuario %>
          </button>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" style="cursor: pointer">Acceso: <%= nivel %></a></li>
            <li><a class="dropdown-item" href="${pageContext.servletContext.contextPath}/login.jsp?cerrar=true">Cerrar Sesion</a></li>
          </ul>
        </div>
                
                
    </div>
</nav>