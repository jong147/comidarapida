<%-- 
    Document   : index
    Created on : Oct 1, 2022, 12:45:41 PM
    Author     : Jong
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%
    HttpSession ses = request.getSession();
    String usuario = "";
    String nivel = "";
    if(ses.getAttribute("usuario") != null && ses.getAttribute("access") != null
    && ses != null){
        usuario = ses.getAttribute("usuario").toString(); //toString();
        nivel = ses.getAttribute("access").toString();
        
//        if(nivel != "administrador"){
//            response.sendRedirect("login.jsp");
//        }
        
    }else{
        response.sendRedirect("login.jsp");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
        <title>Sistema de Ventas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS de Bootstrap -->
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/bootstrap/bootstrap.min.css">
        <!-- JS de Bootstrap -->
        <script src="${pageContext.servletContext.contextPath}/bootstrap/jquery-3.6.1.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/bootstrap/bootstrap.min.js"></script>
    </head>
    <body>
        <!-- Directiva para incluir en el menu -->
        
        <div class="container mt-4">
            <%@ include file="template/menu.jsp" %>
            <h1>Bienvenido <%= usuario %> al Sistema.</h1>
            <hr>
        </div>
    </body>
</html>
