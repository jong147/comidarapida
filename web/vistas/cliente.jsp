<%-- 
    Document   : cliente
    Created on : Oct 1, 2022, 12:39:26 PM
    Author     : Jong
--%>
<%@page import="com.modelo.Cliente" %>
<%@page import="com.modelo.Categoria" %>
<%@page import="com.dao.ClienteDAO" %>
<%@page import="com.dao.CategoriaDAO" %>
<%@page session="true" %>
<%
    HttpSession ses = request.getSession();
    String usuario = "";
    String nivel = "";
    if(ses.getAttribute("usuario") != null && ses.getAttribute("access") != null
    && ses != null){
        usuario = ses.getAttribute("usuario").toString(); //toString();
        nivel = ses.getAttribute("access").toString();
        
//        if(nivel != "administrador"){
//            response.sendRedirect("login.jsp");
//        }
        
    }else{
        response.sendRedirect("../login.jsp");//sin o con puntos
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cliente</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS de Bootstrap -->
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/bootstrap/bootstrap.min.css">
        <!-- JS de Bootstrap -->
        <script src="${pageContext.servletContext.contextPath}/bootstrap/jquery-3.6.1.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/bootstrap/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
    </head>
    <body>       
        <%! CategoriaDAO categoria = new CategoriaDAO();
            ClienteDAO cliente = new ClienteDAO(); %>
        <%@include file="../template/menu.jsp" %>
        <div class="container mt-4">
            <div class="row">
            <div class="col-md-8">
            <h1>Clientes</h1>
            </div>
            <!-- Botón para agregar -->
            <div class="col-md-4 text-right mt-1">
            <button type="button" class="btn btn-primary btnAdd" data-toggle="modal" data-target="#mdlFormulario">
                Agregar cliente
            </button>
            </div>
            </div>
            <hr>
            
            <!-- Tabla -->
            <table class="table mt-4" id="myDataTable">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Código</th>
                        <th scope="col">Cliente</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Categoría</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <% 
                    //ArrayList<Categoria> lista = categoria.mostrarCategorias();
                    for (Cliente elem : cliente.mostrarCliente()) {

                    %>

                    <tr>
                        <td class="codigo"><%= elem.getIdCliente() %></td>
                        <td class="nombre"><%= elem.getNombre() %></td>
                        <td class="edad"><%= elem.getEdad() %></td>
                        <td class="categoria"><%= elem.getCategoria() %></td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success btnEditar" data-toggle="modal" data-target="#mdlFormulario" id="editar">Editar</button>
                                <button type="button" class="btn btn-danger btnEliminar" data-toggle="modal" data-target="#mdlFormulario" id="eliminar">Eliminar</button>
                            </div>
                        </td>
                    </tr>   

                    <% 
                        }
                    %>
                                                        
                </tbody>
            </table>

            <!-- Modal para agregar-->
            <div class="modal fade" id="mdlFormulario">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Cliente</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="${pageContext.servletContext.contextPath}/ClienteServlet" method="POST">
                            <div class="modal-body">
                                Código
                                <input type="text" name="txtCodigo" id="txtCodigo" class="form-control" value="0" readonly>
                                Nombre
                                <input type="text" name="txtNombre" id="txtNombre" class="form-control">
                                Edad
                                <input type="number" name="txtEdad" id="txtEdad" class="form-control">
                                Categoría
                                <select name="txtCategoria" id="txtCategoria" class="form-control">  
                                <option value="">Seleccionar categoría...</option>
                                <% 
                                //ArrayList<Categoria> lista = categoria.mostrarCategorias();
                                for (Categoria elem : categoria.mostrarCategorias()) {
                                    
                                %>
                                
                                <option value="<%= elem.getIdCategoria() %>"><%= elem.getCategoria() %></option>
                                
                                <% 
                                    }
                                %>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary btnOcultar1" name="btnAgregar" id="btnAgregar">Agregar</button>
                                <button class="btn btn-success btnOcultar" name="btnEditar" id="btnEditar">Editar</button>
                                <button class="btn btn-danger btnOcultar" name="btnEliminar" id="btnEliminar">Eliminar</button>
                                <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
        <%
            if (request.getAttribute("message") != null){
        %>                        
        <script>alert('<%= request.getAttribute("message") %>')</script>
        <%
            }
        %>
        
        <script src="${pageContext.servletContext.contextPath}/js/cliente.js"></script>
    </body>
</html>
