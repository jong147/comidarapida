/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.modelo;

/**
 *
 * @author Jong
 */
public class Usuario {
    private int idUser;
    private String username;
    private String contrasena;
    private String nivelAccess;

    public Usuario() {
    }

    public Usuario(int idUser, String username, String contrasena, String nivelAccess) {
        this.idUser = idUser;
        this.username = username;
        this.contrasena = contrasena;
        this.nivelAccess = nivelAccess;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNivelAccess() {
        return nivelAccess;
    }

    public void setNivelAccess(String nivelAccess) {
        this.nivelAccess = nivelAccess;
    }
    
}
