package com.dao;

import com.conexion.Conexion;
import com.modelo.Categoria;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Jong
 */
public class CategoriaDAO extends Conexion {
    
    //funcion para mostar registros
    public ArrayList<Categoria> mostrarCategorias(){
        ArrayList<Categoria> listCategorias = new ArrayList<>();
        try {
            this.conectar();
            String sql = "SELECT * FROM categoria";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            ResultSet rs = pre.executeQuery();
            while(rs.next()){
                Categoria c = new Categoria();
                c.setIdCategoria(rs.getInt(1));
                c.setCategoria(rs.getString(2));
                listCategorias.add(c);
            }
        } catch (SQLException e) {
            System.out.println("Error al mostrar los registros "+e.getMessage());
        }
        finally {
            this.desconectar();
        }
        return listCategorias;
    }
    
//    //funcion para insertar registros
//    public int insertarCategoria(Categoria c){
//        int res = 0;
//        try {
//            this.conectar();
//            String sql ="INSERT INTO categoria(categoria, direccion, telefono) VALUES(?,?,?)";
//            PreparedStatement pre = this.getCon().prepareStatement(sql);
//            pre.setString(1, c.getCategoria());
//            
//            res = pre.executeUpdate();
//        } catch (SQLException e) {
//            System.out.println("Error al agregar registro "+e.getMessage());
//        }
//        return res;
//    }
    
//    //funcion para modificar registros
//    public int modificarCategoria(Categoria c){
//        int res = 0;
//        try {
//            this.conectar();
//            String sql = "UPDATE categoria SET categoria=?, direccion=?, telefono=? WHERE idCategoria=?";
//            PreparedStatement pre = this.getCon().prepareStatement(sql);
//            pre.setString(1, c.getCategoria());
//            pre.setInt(4, c.getIdCategoria());
//            
//            res = pre.executeUpdate();
//        } catch (SQLException e) {
//            System.out.println("Error al modificar "+e.getMessage());
//        }
//        return res;  
//    }
//    
//    //funcion par eliminar registros
//    public int eliminarCategoria(Categoria c){
//        int res = 0;
//        try {
//            this.conectar();
//            String sql = "DELETE FROM categoria WHERE idCategoria=?";
//            PreparedStatement pre = this.getCon().prepareStatement(sql);
//            pre.setInt(1, c.getIdCategoria());
//            
//            res = pre.executeUpdate();
//        } catch (SQLException e) {
//            System.out.println("Error al eliminar "+e.getMessage());
//        }
//        return res;
//    }
}