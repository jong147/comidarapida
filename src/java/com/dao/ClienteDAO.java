package com.dao;

import com.conexion.Conexion;
import com.modelo.Cliente;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Jong
 */

public class ClienteDAO extends Conexion {
    
    //funcion para mostrar clientes
    public ArrayList<Cliente> mostrarCliente(){
        ArrayList<Cliente> listaClientes = new ArrayList <>();
        try {
            this.conectar();
            String sql = "SELECT * FROM cliente cl INNER JOIN categoria ca on cl.idCategoria=ca.idCategoria";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            ResultSet rs = pre.executeQuery();
            while(rs.next()){
            Cliente c = new Cliente();
            c.setIdCliente(rs.getInt(1));
            c.setNombre(rs.getString(2));
            c.setEdad(rs.getInt(3));
            c.setIdCategoria(rs.getInt(4));
            c.setCategoria(rs.getString(6)); // lic puso 6
            listaClientes.add(c);
            }
        }
        catch (SQLException e) {
            System.out.println("Error al mostrar registro"+e.getMessage());
        }
        finally {
           this.desconectar();
        }
        return listaClientes;
    }
    
    //funcion para insertar clientes
    public int insertarCliente(Cliente c) {   
        int res=0;
        try {
            this.conectar();
            String sql = "INSERT INTO cliente(nombre, edad, idCategoria) VALUES(?,?,?)";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setString(1, c.getNombre());
            pre.setInt(2, c.getEdad());
            pre.setInt(3, c.getIdCategoria());
            res = pre.executeUpdate();
        }
        catch (SQLException e) {
            System.out.println("Error al insertar registro"+e.getMessage());
        }
        finally {
            this.desconectar();
        }
        return res;
    }
    
    public int modificarCliente(Cliente c){
        int res =0;
        try {
            this.conectar();
            String sql = "UPDATE cliente set nombre=?, edad=?, idCategoria=? WHERE idCliente=?";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setString(1, c.getNombre());
            pre.setInt(2, c.getEdad());
            pre.setInt(3, c.getIdCategoria());
            pre.setInt(4, c.getIdCliente());
            res = pre.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error al modificar registro: "+e.getMessage());
        } finally{
            this.desconectar();
        }
        return res;
    }
    
    public int eliminarCliente(Cliente c){
        int res=0;
        try {
            this.conectar();
            String sql = "DELETE FROM cliente WHERE idCliente=?";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            pre.setInt(1, c.getIdCliente());
            res = pre.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error al eliminar registro: "+e.getMessage());
        }
        finally{
            this.desconectar();
        }
        return res;
    }
}
